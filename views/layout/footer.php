	<section class="clear"></section>
	<footer>
		<section class="container">
			<section class="row">
				<section class="col-lg-4">
					<h2>
						About Phu Office
					</h2>
					<section class="footer__box">
						<p>
                            Liền Kề 46 Liền kề 47. NO 7D - KĐDV - Phường Vạn Phúc - Hà Đông - Hà Nội
						</p>
						Contact me: <br>
						<a href=""><img src="https://img.icons8.com/doodle/48/000000/facebook-circled.png"/></a>
						<a href=""><img src="https://img.icons8.com/fluent/48/000000/google-logo.png"/></a>
<!--						<a href=""><i class="fa fa-youtube-square" aria-hidden="true"></i></a>-->
<!--						<a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a>-->
					</section>
				</section>
				<section class="col-lg-4">
					<h2>
						Bài viết mới cập nhập
					</h2>
					<section class="footer__box">
						<?php 
						$lastest = \models\Post::where()->orderBy(['post_time', 'desc'])->limit(4)->get();
						foreach ($lastest as $value): ?>
							
						<section class="footer__box__lastest">
							<a href="<?=getUrl('post?id='.$value->id) ?>"><?=$value->title ?></a>
							<span class="posttime"><br>
								<i class="fa fa-clock-o"></i> <?php
								echo convertTime($value->post_time);
							?>
							</span>
						</section>

						<?php endforeach ?>
						
					</section>
				</section>
<!-- 				<section class="col-lg-3">
					<h2>
						Subcribe Email
						
					</h2>
					<p>
						Enter your email to read the lastest news
					</p>
					<form action="#">
						<input type="email" placeholder="Your email..." required="">
						<button>
							<i class="fa fa-rss" aria-hidden="true"></i>
						</button>
					</form>
				</section> -->
				<section class="col-lg-4">
					<h2>
						Danh Mục tin tức
					</h2>
					<?php 
					$categories = \models\Category::all();
					 ?>
					<section class="footer__box__categories">
						<?php foreach ($categories as $value): ?>
						<section class="footer__category">
							<section class="footer__category--left">
								<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>
								<a href="#"><?php echo $value->category_name ?></a>
							</section>
							<span>
								<?php echo $value->countPost() ?>
							</span>
						</section>
						<?php endforeach ?>
					</section>
				</section>
			</section>
		</section>
		<section class="footer__bottom">
			<section class="container">
				<section class="row">
					<section class="footer__bottom__left col-lg-6">
						<a href="#">Homepage</a>
						<a href="#">About</a>
						<a href="#">Contact</a>
					</section>
					<section class="footer__bottom__right col-lg-6">
						© Cover <?php date('Y') ?> By NguyenPhu | Design NguyenPhu
					</section>
				</section>
			</section>
		</section>
	</footer>
</body>
</html>