<?php
date_default_timezone_set('Asia/Ho_Chi_Minh');
//function dd($var){
//	echo "<pre>";
//	var_dump($var);
//	die;
//}
function getUrl($path = ""){
	$currentUrlpath = $GLOBALS['url'];
	$absoluteUrl = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	if($currentUrlpath != "/"){
		$absoluteUrl = str_replace("$currentUrlpath", "", $absoluteUrl);
	}
	$absoluteUrl = substr($absoluteUrl, 0, strrpos($absoluteUrl ,'/'));
	return $absoluteUrl.'/'.$path;
}
function convertTime($str){
$date = new \DateTime($str);
return $date->format('d F Y');
}
function config(){
	return [
		'name' => 'Nguyen Doan Phu',
		'cms_name' => 'Phu Office',
	];
}
function d($data){
	if(is_null($data)){
		$str = "<i>NULL</i>";
	}elseif($data == ""){
		$str = "<i>Empty</i>";
	}elseif(is_array($data)){
		if(count($data) == 0){
			$str = "<i>Empty array.</i>";
		}else{
			$str = "<table style=\"border-bottom:0px solid #000;\" cellpadding=\"0\" cellspacing=\"0\">";
			foreach ($data as $key => $value) {
				$str .= "<tr><td style=\"background-color:#008B8B; color:#FFF;border:1px solid #000;\">" . $key . "</td><td style=\"border:1px solid #000;\">" . d($value) . "</td></tr>";
			}
			$str .= "</table>";
		}
	}elseif(is_resource($data)){
		while($arr = mysql_fetch_array($data)){
			$data_array[] = $arr;
		}
		$str = d($data_array);
	}elseif(is_object($data)){
		$str = d(get_object_vars($data));
	}elseif(is_bool($data)){
		$str = "<i>" . ($data ? "True" : "False") . "</i>";
	}else{
		$str = $data;
		$str = preg_replace("/\n/", "<br>\n", $str);
	}
	return $str;
}
function description($desc, $length)
{
	$desc = strip_tags ($desc) ;
	if (strlen($desc)>$length) {
	  $desc = substr($desc, 0, $length);
	}
	echo $desc;
	}
 ?>

